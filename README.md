# Qubits und Kryptographie
Diese Skripte wurden erstellt, um Eigenschaften quantenmechanischer Zustände, speziell auf einzelnen Qubits, zu veranschaulichen.
In `messung.py` wird eine Messung in einem Stern-Gerlach-Experiment mit verschiedenen Anfangszuständen und Messachsen simuliert.
In `crypto.py` wird ein einfaches Protokoll zum Austauschen von Schlüsseln über verschränkte Zustände (quantum key sharing) mit und ohne Abhören simuliert.

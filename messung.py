#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt


class Plot:
    """
    Klasse zur Darstellung von simulierten Messergebnissen
    """
    def __init__(self, sample_size=1000):
        """
        Diese Funktion wird beim Erstellen eines Objects der Klasse
        aufgerufen ("Konstruktor"). Hier wird die grafische Oberflaeche
        erstellt.
        """
        self.sample_size = sample_size
        # Erstelle ein Fenster mit matplotlib
        self.figure = plt.figure()
        # Erstelle eine Achse fuer das Histogram, das spaeter gezeigt werden
        # soll
        self.histogram_axis = self.figure.add_axes((0.15, 0.4, 0.3, 0.5))
        self.histogram_axis.set_xlabel("Messergebnis")
        self.histogram_axis.set_ylabel("Haeufigkeit des Ergebnisses")
        # Erstelle eine Achse, auf der der Winkel fuer die Messung
        # eingestellt werden kann.
        self.measurement_angle_axis = self.figure.add_axes((0.6, 0.5, 0.3, 0.3))
        self.measurement_angle_axis.set_aspect(1)
        self.measurement_angle_axis.set_xlim(-1, 1)
        self.measurement_angle_axis.set_ylim(-1, 1)
        self.measurement_angle_axis.xaxis.set_visible(False)
        self.measurement_angle_axis.yaxis.set_visible(False)
        # Erstelle ein Textfeld unter der Achse, in dem der Winkel angezeigt
        # wird.
        self.measurement_angle_text = self.figure.text(
                0.55, 0.45,
                "Messung mit Winkel = 0°",
                fontdict = dict(size=14))

        # Erstelle ein Textfeld vor den Knoepfen zur Auswahl des
        # Anfangszustands.
        self.figure.text(0.02, 0.08, "Anfangszustand:", fontdict=dict(size=14))
        # Erstelle einen Knopf fuer Anfangszustand "0":
        zero_button_ax = self.figure.add_axes((0.35, 0.05, 0.05, 0.05))
        self.initial_state_zero = plt.Button(zero_button_ax, label="0")
        self.initial_state_zero.on_clicked(self.set_initial_state_zero)
        # Erstelle einen Knopf fuer Anfangszustand "+":
        one_button_ax = self.figure.add_axes((0.425, 0.05, 0.05, 0.05))
        self.initial_state_one = plt.Button(one_button_ax, label="1")
        self.initial_state_one.on_clicked(self.set_initial_state_one)
        # Erstelle einen Knopf fuer Anfangszustand "+":
        plus_button_ax = self.figure.add_axes((0.5, 0.05, 0.05, 0.05))
        self.initial_state_plus = plt.Button(plus_button_ax, label="+")
        self.initial_state_plus.on_clicked(self.set_initial_state_plus)
        # Erstelle einen Knopf fuer Anfangszustand "-":
        minus_button_ax = self.figure.add_axes((0.575, 0.05, 0.05, 0.05))
        self.initial_state_minus = plt.Button(minus_button_ax, label="-")
        self.initial_state_minus.on_clicked(self.set_initial_state_minus)

        # Verknuepfe Funktionen zum Verwenden von Eingabeereignissen.
        # Diese werden gebraucht, um den Winkel der Messung per Maus
        # auszuwaehlen.
        self.figure.canvas.mpl_connect(
                "button_press_event",
                self.on_mouse_press)
        self.figure.canvas.mpl_connect(
                "button_release_event",
                self.on_mouse_release)
        self.figure.canvas.mpl_connect(
                "motion_notify_event",
                self.mouse_drag)
        self.drag_angle = False

        # Initialisiere die Winkel fuer Praeparierung und Messung.
        self.measurement_angle = 0.
        # Erstelle eine Pfeil, der den Winkel der Messung anzeigt.
        self.measurement_arrow = self.measurement_angle_axis.arrow(
                0, 0, 1, 0,
                head_width = 0.08,
                head_length = 0.08,
                length_includes_head = True)
        # Setze den Anfangszustand auf "0"
        self.set_initial_state_zero(None)

    def on_mouse_press(self, event):
        """
        Registriere wenn auf die Maustasten gedrueckt wird. Falls auf
        der Achse zum Anpassen des Messwinkels geklickt wird, wird das
        Aendern des Messwinkels per Maus aktiviert.
        """
        if event.inaxes == self.measurement_angle_axis:
            self.drag_angle = True
            self.mouse_drag(event)

    def on_mouse_release(self, event):
        """
        Loslassen der Maustasten: Das Aendern des Messwinkels per Maus
        wird deaktiviert.
        """
        self.drag_angle = False

    def mouse_drag(self, event):
        """
        Ziehen der Maus ueber das Fenster: Passe den Messwinkel an,
        falls eine Maustaste gedrueckt wurde und die Maus ueber die
        Achse zum Anpassen des Messwinkels gezogen wird.
        Dabei wird auch der Pfeil und der Text zum Anzeigen des
        Messwinkels neu erstellt bzw. angepasst.
        Zuletzt wird das Histogramm neu erstellt
        """
        if self.drag_angle and event.inaxes == self.measurement_angle_axis:
            self.measurement_angle = np.arctan2(event.ydata, event.xdata)
            self.measurement_angle_text.set_text(
                    "Messung mit Winkel = %.1f°"
                    % (180*self.measurement_angle/np.pi))
            try:
                self.measurement_arrow.remove()
            except AttributeError:
                pass
            self.measurement_arrow = self.measurement_angle_axis.arrow(
                    0, 0,
                    np.cos(self.measurement_angle),
                    np.sin(self.measurement_angle),
                    head_width = 0.08,
                    head_length = 0.08,
                    length_includes_head = True)
            self.update_histogram()

    def update_histogram(self):
        """
        Erstelle das Histogramm neu, mit dem die simulierte Messung
        dargestellt wird.
        """
        try:
            self.histogram.remove()
        except AttributeError:
            pass
        # Berechne die Wahrscheinlichkeit fuer Messergebnis +1.
        angle = self.measurement_angle - self.preparation_angle
        probability_plus = np.cos(angle/2)**2
        # Erstelle ein array mit Werten True und False entsprechend den
        # Wahrscheinlichkeiten fuer Messergebnis +1 und -1.
        random_array = np.random.random(size=self.sample_size) < probability_plus
        # Rechne von True und False um auf die Zahlen +1 und -1.
        random_samples = np.float_(2*random_array-1)
        # Fuege etwas Rauschen hinzu, damit es eher wie ein echtes Experiment
        # aussieht.
        random_samples += np.random.normal(scale=0.03, size=self.sample_size)
        # Erstelle aus den simulierten Daten ein Histogramm.
        y, x, histogram = self.histogram_axis.hist(
                random_samples,
                bins = 25,
                range = (-1.25, 1.25),
                color = "blue")
        self.histogram = histogram
        # Aktualisiere die grafische Darstellung.
        self.figure.canvas.draw_idle()

    def set_initial_state_zero(self, event):
        """
        Setze den Anfangszustand auf "0".
        """
        self.preparation_angle = 0.
        self.initial_state_zero.color = "red"
        self.initial_state_one.color = "gray"
        self.initial_state_plus.color = "gray"
        self.initial_state_minus.color = "gray"
        self.update_histogram()

    def set_initial_state_one(self, event):
        """
        Setze den Anfangszustand auf "1".
        """
        self.preparation_angle = np.pi
        self.initial_state_zero.color = "gray"
        self.initial_state_one.color = "red"
        self.initial_state_plus.color = "gray"
        self.initial_state_minus.color = "gray"
        self.update_histogram()

    def set_initial_state_plus(self, event):
        """
        Setze den Anfangszustand auf "+".
        """
        self.preparation_angle = np.pi/2
        self.initial_state_zero.color = "gray"
        self.initial_state_one.color = "gray"
        self.initial_state_plus.color = "red"
        self.initial_state_minus.color = "gray"
        self.update_histogram()

    def set_initial_state_minus(self, event):
        """
        Setze den Anfangszustand auf "-".
        """
        self.preparation_angle = 3*np.pi/2
        self.initial_state_zero.color = "gray"
        self.initial_state_one.color = "gray"
        self.initial_state_plus.color = "gray"
        self.initial_state_minus.color = "red"
        self.update_histogram()
        

if __name__ == "__main__":
    Plot()
    plt.show()

#!/usr/bin/env python
import numpy as np


def main(eve=True, size=50):
    """
    Simuliere das Austauschen von Schlüsseln durch das Nutzen
    verschränkter Quantenzustände.
    """
    # Winkel beim Senden der Qubits: Zufällig entweder 0 oder π/2 (90°)
    alice_angle = np.pi/2 * np.random.randint(0, 2, size=size)
    # Bits, die Alice senden will: zufällig, 0 oder 1
    alice_bits = np.random.randint(0, 2, size=size)
    # Winkel beim Messen der Qubits: Zufällig entweder 0 oder π/2 (90°)
    bob_angle = np.pi/2 * np.random.randint(0, 2, size=size)

    if eve:
        # Messwinkel für Eve: zufällig entweder 0 oder π/2 (90°)
        eve_angle = np.pi/2 * np.random.randint(0, 2, size=size)
        eve_prob = np.cos((alice_angle + np.pi*alice_bits - eve_angle)/2)**2
        eve_bits = np.int_(np.random.random(size=size) >= eve_prob)
        bob_prob = np.cos((eve_angle + np.pi*eve_bits - bob_angle)/2)**2
    else:
        bob_prob = np.cos((alice_angle + np.pi*alice_bits - bob_angle)/2)**2
    bob_bits = np.int_(np.random.random(size=size) >= bob_prob)

    select_bits = alice_angle == bob_angle
    alice_password = alice_bits[select_bits]
    bob_password = bob_bits[select_bits]
    print("Alice:", alice_password)
    print("Bob:  ", bob_password)
    if eve:
        eve_password = eve_bits[select_bits]
        print("Eve:  ", eve_password)
    print("Anteil abweichender Bits bei Alice und Bob:", (alice_password != bob_password).sum() / alice_password.size)
    return (alice_password != bob_password).sum()


if __name__ == "__main__":
    print("Ohne Abhören:")
    main(eve=False)
    print("Mit Abhören:")
    main(eve=True)
